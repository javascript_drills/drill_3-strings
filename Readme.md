# Drill on Strings in Javascript

## This drill contains various operations on strings using various inbuild functions.

### Following are the Operations performed on Strings

1. Conversion of a amout in string to a Number format without loss in its precision.

2. Conversion of string of IPV4 IP address into components and returning in array format.

3. Extraction of month from string receieved in  Date foramt. 

4. Conversion of Names receieved to title case.

5. concating strings reveieved from array to a complete String.