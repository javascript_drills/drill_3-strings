
const toTitleCase = userObject =>  Object.values(userObject).map(name => name.charAt(0).toUpperCase() + name.substr(1).toLowerCase()).join(" ").trim();

module.exports = toTitleCase;