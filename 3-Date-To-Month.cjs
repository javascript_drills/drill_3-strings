
 const Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

const dateToMonth = date1 => typeof date1 === "string" ?  (Months[Number(date1.split("/")[1])-1]) : undefined  ;

module.exports = dateToMonth ;