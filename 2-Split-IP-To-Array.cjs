
let IPV4Regex = /^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/gm;

const splitIP = Ip => IPV4Regex.test(Ip)? Ip.split("."):[] ;

module.exports = splitIP;