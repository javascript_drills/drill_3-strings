
const convertToAmount = amount => typeof amount === "string" ? (+amount.trim().replaceAll("," , "").replaceAll("$","")) || 0 :undefined ;

module.exports = convertToAmount;